-- Style tables should have the following structure:
-- Name = {
--     Color = {
--         ['Flag 1'] = {R,G,B,A},
--         ['Flag 2'] = {R,G,B,A}
--     }
--     Style = {
--         ['Flag 3'] = {v1},
--         ['Flag 4'] = {v1, v2}
--     }
-- }

return {
    -- Hack to make center aligned text with full window width buttons
    transparentButton = {
        Color = {
            ['Button'] = {
                GUI:GetStyle().colors[GUI.Col_Button][1],
                GUI:GetStyle().colors[GUI.Col_Button][2],
                GUI:GetStyle().colors[GUI.Col_Button][3],
                0
            },
            ['ButtonHovered'] = {
                GUI:GetStyle().colors[GUI.Col_ButtonHovered][1],
                GUI:GetStyle().colors[GUI.Col_ButtonHovered][2],
                GUI:GetStyle().colors[GUI.Col_ButtonHovered][3],
                0
            },
            ['ButtonActive'] = {
                GUI:GetStyle().colors[GUI.Col_ButtonActive][1],
                GUI:GetStyle().colors[GUI.Col_ButtonActive][2],
                GUI:GetStyle().colors[GUI.Col_ButtonActive][3],
                0
            }
        },
        Style = { }
    },

    -- Buttons to use as tab selection
    tabHeaderButton = {
        Color = {
            ['Button'] = { 0, 0, 0, 0.05 },
            ['ButtonHovered'] = { 1, 1, 1, 0.2 },
            ['ButtonActive'] = { 1, 1, 1, 0.3 }
        },
        Style = {
            ['WindowPadding'] = {0, 0},
            ['FrameRounding'] = {0},
            ['ItemSpacing'] = {0, GUI:GetStyle().itemspacing.y}
        }
    },

    tabHeaderButtonSelected = {
        Color = {
            ['Button'] = { 0.15, 0.5, 0.3, 1 },
            ['ButtonHovered'] = { 0.15, 0.5, 0.3, 1 },
            ['ButtonActive'] = { 0.15, 0.5, 0.3, 1 }
        },
        Style = {
            ['WindowPadding'] = {0, 0},
            ['FrameRounding'] = {0},
            ['ItemSpacing'] = {0, GUI:GetStyle().itemspacing.y}
        }
    },

    enabledToggle = {
        Color = {
            ['Button'] = {0.1, 0.8, 0.1, 1},
            ['ButtonHovered'] = {0.1, 0.8, 0.1, 1},
            ['ButtonActive'] = {0.1, 0.8, 0.1, 1},
        },
    },

    disabledToggle = {
        Color = {
            ['Button'] = {0.8, 0.1, 0.1, 1},
            ['ButtonHovered'] = {0.8, 0.1, 0.1, 1},
            ['ButtonActive'] = {0.8, 0.1, 0.1, 1},
        },
    },
}