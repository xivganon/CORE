return {
    {
        name = "Discord",
        icon = GetLuaModsPath() .. [[AACore/icons/discord.png]],
        link = [[https://discord.gg/qfbAvcbutT]],
        text = [[If you need support on using my modules, or just want to chat, check the Discord!]]
    },
    {
        name = "Ko-fi",
        icon = GetLuaModsPath() .. [[AACore/icons/kofi.png]],
        link = [[https://ko-fi.com/xivg_anon]],
        text = [[Want me to keep forgoing sleep to make these modules? Help me with some cofee!]],
    },
    {
        name = "Trello",
        icon = GetLuaModsPath() .. [[AACore/icons/trello.png]],
        link = [[https://trello.com/invite/b/jdWwg7E8/57c4e07351c45a3b824822fe3d37826b/anon-addons]],
        text = [[Check Trello to see what is planned, and vote on features that I should prioritize!]],
    },
}