# Anon Addons

This is the core module for @xivganon's addons. It serves as an entry point for all the other modules, hosts smaller tweaks and has facilities to keep track of the other modules updates. It is also where you can find most of the info about my work.

If you enjoy what I do, consider [donating through Ko-Fi](https://ko-fi.com/xivg_anon)  :coffee:. I love working on these, and having financial support helps me to free time so I can do more.

If you have a problem with one of the modules, you can [create an issue on this project](https://gitlab.com/xivganon/CORE/-/issues) :bug: or on the specific project page. Or if you'd rather try for live support or just have a chat, [join me on discord](https://discord.gg/qfbAvcbutT) :speech_balloon:!

## [Installation](#installation)

Download the latest release from the [release page](https://gitlab.com/xivganon/CORE/-/releases/) and get the zip on the **other** section.

![ReleaseLink](https://i.ibb.co/t2pYMyL/image.png "Example")

After that, extract the zip to your LuaMods or BaseProfile folder like so, ensuring it has it's own folder:

![Extraction](https://i.ibb.co/nnS253M/image.png "Into the folder it goes")

Folder structure should end up like:

![Folders](https://i.ibb.co/4JRqJKY/image.png "Each module has its own folder")

The same process should apply to all my other released modules

# Released modules:

##  :package: **[Leve Delivery](https://gitlab.com/xivganon/LEDE)**

> ### Automatic levequest turn in
> Create a list of levequests, and if you have the necessary items it will pick up the levequest and turn them in for you
