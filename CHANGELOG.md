# CHANGELOG

Documented changes for the AnonAddons Core Module

## [Unreleased]

## [1.1.0] - 2022-01-14

### Added
- Added TextureButton helper.
- Added TextureToggle helper.
- Added a Core sub-button since no one ever noticed the 'AnonAddons' menu entry is clickable.

## [1.0.1] - 2021-01-24

### Fixed

- Fix the whole UI breaking on core due to a forgotten `GUI:PopItemWidth()` (how did I miss this)

## [1.0.0] - 2021-01-23

### Added

- Add about section with links for discord / kofi / trello.
- Add settings tab (empty for now).
- Add event listener for finished initialization of other modules.
- Add `aa.ChangeBotMode(mode: string)` to help change the current bot mode.
- Add `aa.SaveSettings(moduleName: string)` to help save the settings table on minion DB.
- Add `aa.PushStyle(style: table)` and `aa.PopStyle(style: table)` to help quickly change layouts.
- Add `aa.DrawStartStopButton(modeName: string, modeTag: string, width: number, height: number)` which is a button with all the logic required to change modes and swap modes without having to press again for the bot to continue.
- Add `aa.FindIndex(tab: table, fn: function)` to complement `aa.Find(tab: table, fn: function)`.
- Add `aa.SendHttpRequest(host: string, path: string, method: string, body: string, headers: string, parcel: any, succesHandler: function, failureHandler: function)`, slightly modified from the [Minion Wiki](http://wiki.mmominion.com/doku.php?id=minionlib).
- Add `aa.GitlabRequest(method: string, endpoint: string, query: string, sHandler: function, fHandler: function)` as a smaller wrapper for gitlab api requests.

### Changed

- New UI! Pretty tabs!
- Simplified `aa.Require(filePath:string)` logic.
- Completely revamped the core module.
- Module versioning is now independent from the core module.
- Core module changelog is based on the CORE.lua file, and any aa.lua (helper/global module) changes will be listed as minor updates.

---

## [0.4.3] - 2020-08-14

### Fixed

- Fixed the checkbox confirmation for real this time

---

## [0.4.2] - 2020-08-13

### Added

- Added support for selection UI when handling simultaneous deliveries
- Added support for the high-quality checkbox confirmation

### Fixed

- Fixed 'petalite bracelet of fending' entry not marked as a multiple delivery leve

---

## [0.4.1] - 2020-08-02

### LeveDelivery

#### Changed

- Removed debug button from main task window

---

## [0.4.0] - 2020-08-02

### Added

- Leve Delivery module! Can deliver levequest items automatically. Only support for two leves for now, but can be easily expanded

### Changed

- Formatting and commenting in various sections

### Removed

- Disabled EliteHunter from the main package for now as it likely needs a complete rework now that I have a much better understanding of CnD and have adopted a new standard for formatting them, removing them from the global table
- Removed Acelib dependency

---

## [0.3.1]

### Added

- Made a ghetto "require" function

### Changed

- Separated helper functions on aa.
- Moved extra contents to /sounds, /images and /libs
- Registers for extra modules are now on the AnonAddons.lua file
- Moved swap function to aa

### Removed

- Removed Modules from module.def and delete their module.def

---

## [0.3.0] - 2019-09-22

### Added

- Added a changelog!

### Changed

- Updated HUDSwitch to 1.2.0

### Removed
