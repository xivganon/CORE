local AA = {}

-- -------------------------------------------------------------------------- --
-- ANCHOR                         Internal Info                               --
-- -------------------------------------------------------------------------- --

AA._VERSION = "1.1.0"
AA._NAME = "Anon Addons"
AA._DESCRIPTION = "/xivg/anon\'s modules for FFXIVMinion"
AA._URL = "https://gitlab.com/xivganon/CORE"
AA._ID = "14320608"

d("[AnonAddons] Core loaded")

-- -------------------------------------------------------------------------- --
-- ANCHOR                       Module variables                              --
-- -------------------------------------------------------------------------- --

AA.GUI = {
	open = false,
	visible = false,
	tabs = {
		"About",
		"Settings",
		"Updater",
	},
	tabSelected = 1,
}

AA.modules = {
	LEDE = nil,
	QTRM = nil,
	HUDS = nil,
	SUPP = nil,
	MANU = nil,
}

AA.settings = {}

-- -------------------------------------------------------------------------- --
-- ANCHOR                        Initialization                               --
-- -------------------------------------------------------------------------- --

function AA.Init()
	d("[AnonAddons] Core Initializing")

	aa["Layouts"] = aa.Require(GetLuaModsPath() .. [[AACore/layouts.lua]])
	aa["Links"] = aa.Require(GetLuaModsPath() .. [[AACore/links.lua]])
	aa["Icons"] = {
		trash = GetLuaModsPath()..[[AACore/icons/trash.png]]
	}

	local menuIcon = GetLuaModsPath() .. [[AACore/AA.png]]

	ml_gui.ui_mgr:AddMember(
		{
			id = "AA##AA",
			name = "Anon Addons",
			texture = menuIcon,
			onClick = nil,
		},
		"FFXIVMINION##MENU_HEADER"
	)
	ml_gui.ui_mgr:AddSubMember(
		{
			id = "CORE##CORE",
			name = "Core",
			texture = nil,
			onClick = AA.Open,
		},
		"FFXIVMINION##MENU_HEADER",
		"AA##AA"
	)
end

function AA.SetModuleInfo(event, moduleTag)
	d(event)
	AA.modules[moduleTag] = true
end

-- -------------------------------------------------------------------------- --
-- ANCHOR                           Rendering                                 --
-- -------------------------------------------------------------------------- --

--- GUI display toggle
-- @function Open
function AA.Open()
	AA.GUI.open = true
end

function AA.Draw(event, ticks)
	if AA.GUI.open then
		GUI:SetNextWindowPosCenter(GUI.SetCond_FirstUseEver)
		GUI:SetNextWindowSize(500, 0, GUI.SetCond_Always)
		AA.GUI.visible, AA.GUI.open = GUI:Begin("Anon Addons##AAWindow", AA.GUI.open)
		GUI:SetItemAllowOverlap()
		if AA.GUI.visible then
			local fullWidth = GUI:GetContentRegionAvail()

-- ---------------------------------- Tabs ---------------------------------- --

			aa.PushStyle(aa.Layouts.tabHeaderButton)
			GUI:BeginChild("##AATabs", 0, 30, true, GUI.WindowFlags_AlwaysAutoResize)
			for index, tabName in pairs(AA.GUI.tabs) do
				if AA.GUI.tabSelected == index then
					aa.PushStyle(aa.Layouts.tabHeaderButtonSelected)
					if GUI:Button(tabName, fullWidth / table.size(AA.GUI.tabs), 30) then
						AA.GUI.tabSelected = index
					end
					aa.PopStyle(aa.Layouts.tabHeaderButtonSelected)
				else
					if GUI:Button(tabName, fullWidth / table.size(AA.GUI.tabs), 30) then
						AA.GUI.tabSelected = index
					end
				end
				GUI:SameLine()
			end
			GUI:NewLine()
			GUI:EndChild()
			aa.PopStyle(aa.Layouts.tabHeaderButton)

-- ---------------------------------- About --------------------------------- --

			if AA.GUI.tabSelected == 1 then
				GUI:Dummy(400, 10)
				aa.PushStyle(aa.Layouts.transparentButton)
				GUI:Button("     Thanks for checking out my modules.\nIf you enjoy what I do, check the links below!",
					fullWidth, 40
				)
				aa.PopStyle(aa.Layouts.transparentButton)
				for index, link in pairs(aa.Links) do
					GUI:BeginChild('##AALinks'..index,(fullWidth/3)-5,150,true,
						GUI.WindowFlags_NoScrollbar+GUI.WindowFlags_AlwaysAutoResize
					)
					GUI:Dummy(GUI:GetContentRegionAvail()/2 - 30, 40)
					GUI:SameLine()
					if GUI:ImageButton('##AAButtonLink'..index,link.icon,40,40) then
						io.popen([[cmd /c start "" "]]..link.link..[["]]):close()
					end
					if GUI:IsItemHovered() then
						GUI:SetTooltip(link.link)
					end
					GUI:TextWrapped(link.text)
					GUI:EndChild()
					GUI:SameLine()
				end
			end

-- -------------------------------- Settings -------------------------------- --

			if AA.GUI.tabSelected == 2 then
				GUI:Dummy(400, 10)
			end

-- --------------------------------- Updater -------------------------------- --

			if AA.GUI.tabSelected == 3 then
				GUI:Dummy(400, 10)

				aa.PushStyle(aa.Layouts.transparentButton)
				GUI:Button(
					string.format("%-8s", "Module") .. " | "
					.. string.format("%11s", "Version") .. " | "
					.. string.format("%11s", "Latest"),
					fullWidth,
					18
				)
				GUI:Separator()
				GUI:Button(
					string.format("%-8s", "CORE") .. " | "
					.. string.format("%11s", AA._VERSION) .. " | "
					.. string.format("%11s", "N/A"),
					fullWidth,
					18
				)
				for moduleName, present in pairs(AA.modules) do
					if (present) then
						GUI:Button(
							string.format("%-8s", moduleName) .. " | "
							.. string.format("%11s", aa[moduleName].GetInfo().Version) .. " | "
							.. string.format("%11s", aa[moduleName].latest or "N/A"),
							fullWidth,
							18
						)
					end
				end
				aa.PopStyle(aa.Layouts.transparentButton)
				GUI:Dummy(400, 10)
				if GUI:Button('Check for Updates', fullWidth, 18) then
					for moduleName, present in pairs(AA.modules) do
						-- if (present) then
						-- 	if(moduleName == "LEDE") then
						-- 		aa.GetAnonModuleLatestVersion(moduleName)
						-- 	end
						-- end
					end
				end
				if GUI:IsItemHovered() then
					GUI:SetTooltip("Under construction!")
				end
			end

		end
		GUI:End()
	end
end

-- -------------------------------------------------------------------------- --
-- ANCHOR                    Registration and Return                          --
-- -------------------------------------------------------------------------- --

RegisterEventHandler("Module.Initalize", AA.Init, "[AnonAddons] CORE Init")
RegisterEventHandler("Gameloop.Draw", AA.Draw, "[AnonAddons] CORE Draw")
RegisterEventHandler("AnonAddons.InitDone", AA.SetModuleInfo, "[AnonAddons] Module List Updated")
