--- AnonAddon library
-- @module aa
aa = {}
aa._TOKEN = "NxNGrF4EpAx_zthTx_Wm"
aa.char_pattern = ".[\128-\191]*"

-- -------------------------------------------------------------------------- --
-- ANCHOR                        Minion Helpers                               --
-- -------------------------------------------------------------------------- --

--- Helper to change the current bot mode.
-- @tparam string botMode Mode name as it is written on the Main Task dropdown
function aa.ChangeBotMode(botMode)
	ffxivminion.SwitchMode(botMode)
	local uuid = GetUUID()
	gBotModeIndex = table.find(gBotModeList, botMode)
	gBotMode = botMode
	if ( string.valid(uuid) ) then
		if  ( Settings.FFXIVMINION.gBotModes == nil ) then Settings.FFXIVMINION.gBotModes = {} end
		Settings.FFXIVMINION.gBotModes[uuid] = botMode
		-- Settings.FFXIVMINION.gBotModes = Settings.FFXIVMINION.gBotModes
	end
end

function aa.GetBagSnapshot()
	bag = {}
	bag = table.merge(bag, Inventory:Get(0):GetList(), true)
	bag = table.merge(bag, Inventory:Get(1):GetList(), true)
	bag = table.merge(bag, Inventory:Get(2):GetList(), true)
	bag = table.merge(bag, Inventory:Get(3):GetList(), true)
	return bag
end


-- -------------------------------------------------------------------------- --
-- ANCHOR                         File Helpers                                --
-- -------------------------------------------------------------------------- --

--- Save table of settings into minion database.
-- @tparam string moduleName Module name
-- @tparam table settingsTable Table with settings value to be saved
-- @usage
-- MyModule.settings = {
--     rate = 10
-- }
-- MyModule.settings.rate = 5555
-- aa.SaveSettings('MyModule', MyModule.settings)
function aa.SaveSettings(moduleName, settingsTable)
	for entry, value in pairs(settingsTable) do
		Settings[moduleName][entry] = value
	end
end

--- Loads and run a lua file
-- @string filePath Full file path
-- @treturn variant Code return
function aa.Require(filePath)
	return assert(loadfile(filePath))()
end

-- -------------------------------------------------------------------------- --
--  ANCHOR                         GUI Helpers                                --
-- -------------------------------------------------------------------------- --

--- Push style table. The table is composed of two tables:
-- one with the color key/value pairs, and one with the style key/value pairs.
-- All subsequent widget wills have the style applied.
-- @tparam table styleTable Table with style definitions.
-- @usage -- example of style table
-- aa.TableExample = {
--     Name = {
--         Color = {
--             ['Flag 1'] = {R,G,B,A},
--             ['Flag 2'] = {R,G,B,A}
--         }
--         Style = {
--             ['Flag 3'] = {v1},
--             ['Flag 4'] = {v1, v2}
--         }
--     }
-- }
-- aa.PushStyle(TableExample)
function aa.PushStyle(styleTable)
	if table.valid(styleTable.Color) then
		for flag, color in pairs(styleTable.Color) do
			GUI:PushStyleColor(_G["GUI"]["Col_" .. flag], color[1], color[2], color[3], color[4])
		end
	end
	if table.valid(styleTable.Style) then
		for flag, variable in pairs(styleTable.Style) do
			if table.size(variable) > 1 then
				GUI:PushStyleVar(_G["GUI"]["StyleVar_" .. flag], variable[1], variable[2])
			else
				GUI:PushStyleVar(_G["GUI"]["StyleVar_" .. flag], variable[1])
			end
		end
	end
end

--- Pop style table. The table is composed of two tables:
-- one with the color key/value pairs, and one with the style key/value pairs.
-- This command will pop the number of entries present on the table to reset the style
-- @tparam table styleTable
function aa.PopStyle(styleTable)
	if table.valid(styleTable.Color) then
		GUI:PopStyleColor(table.size(styleTable.Color))
	end
	if table.valid(styleTable.Style) then
		GUI:PopStyleVar(table.size(styleTable.Style))
	end
end

function aa.DrawSidebar(links)
end

--- draw bigass start stop button
function aa.DrawStartStopButton(modeName, modeTag, width, height)
	if gBotMode == modeName and FFXIV_Common_BotRunning then
		if (GUI:Button('Stop##'..modeTag..'StartStop', width, height)) then
			ml_global_information.ToggleRun()
		end
	elseif FFXIV_Common_BotRunning then
		if (GUI:Button('Swap##'..modeTag..'StartStop', width, height)) then
			aa.ChangeBotMode(modeName)
			ml_global_information.ToggleRun()
		end
	elseif gBotMode == modeName then
		if (GUI:Button('Start##'..modeTag..'StartStop', width, height)) then
			ml_global_information.ToggleRun()
		end
	else
		if (GUI:Button('Start##'..modeTag..'StartStop', width, height)) then
			aa.ChangeBotMode(modeName)
			ml_global_information.ToggleRun()
		end
	end
end

function aa.DrawTabs(identifier, tabInfoTable, fullWidth, height)
	aa.PushStyle(aa.Layouts.tabHeaderButton)
	GUI:BeginChild(identifier, 0, height, true, GUI.WindowFlags_AlwaysAutoResize)
	for index, tabName in pairs(tabInfoTable.tabs) do
		if tabInfoTable.tabSelected == index then
			aa.PushStyle(aa.Layouts.tabHeaderButtonSelected)
			if GUI:Button(tabName, fullWidth / table.size(tabInfoTable.tabs), height) then
				tabInfoTable.tabSelected = index
			end
			aa.PopStyle(aa.Layouts.tabHeaderButtonSelected)
		else
			if GUI:Button(tabName, fullWidth / table.size(tabInfoTable.tabs), height) then
				tabInfoTable.tabSelected = index
			end
		end
		GUI:SameLine()
	end
	GUI:NewLine()
	GUI:EndChild()
	aa.PopStyle(aa.Layouts.tabHeaderButton)
end

function aa.TextureButton(imagePath, sX, sY, v0X, v0Y, vMX, vMY,
	bgR, bgG, bgB, bgA, tR, tG, tB, tA)  
	GUI:Image(imagePath, sX, sY, v0X or 0, v0Y or 0, vMX or 1, vMY or 1,	
	bgR or 1, bgG or 1, bgB or 1, bgA or 1, tR or 0, tG or 0, tB or 0, tA or 0)
	if GUI:IsItemClicked() then
		return true
	end
	return false
end

function aa.TextureToggle(toggled, imagePath, sX, sY, v0X, v0Y, vMX, vMY, 
  bgR, bgG, bgB, bgA, tR, tG, tB, tA, ttR, ttG, ttB, ttA)
  if toggled then
    return aa.TextureButton(imagePath, sX, sY, v0X or 0, v0Y or 0, vMX or 1, vMY or 1,	
    bgR or 1, bgG or 1, bgB or 1, bgA or 1, ttR or 0.1, ttG or 0.9, ttB or 0.1, ttA or 1)
  else
    return aa.TextureButton(imagePath, sX, sY, v0X or 0, v0Y or 0, vMX or 1, vMY or 1,	
	  bgR or 1, bgG or 1, bgB or 1, bgA or 1, tR or 0, tG or 0, tB or 0, tA or 0.001)
  end
end

-- -------------------------------------------------------------------------- --
-- ANCHOR                        String Helpers                               --
-- -------------------------------------------------------------------------- --

function aa.WrapText(text, width)
	local tail, lines = text.." ", {}
	while tail do
	   lines[#lines + 1], tail = tail
		  :gsub("^%s+", "")
		  :gsub(aa.char_pattern, "\0%0\0", width)
		  :gsub("%z%z", "")
		  :gsub("(%S)%z(%s)", "%1%2\0")
		  :gsub("^(%z[^\r\n%z]*)%f[%s](%Z*)%z(.*)$", "%1\0%2%3")
		  :match"^%z(%Z+)%z(.*)$"
	end
	return table.concat(lines, "\n")
 end

-- -------------------------------------------------------------------------- --
-- ANCHOR                         Table Helpers                               --
-- -------------------------------------------------------------------------- --

--- swap entries on a table
function aa.Swap(tab, old, new)
	if (assert(tab)) then
		table.insert(tab, new, table.remove(tab, old))
	end
end

--- run a function on each entry of the table and return a new table
function aa.Map(tab, fn)
	local newTab = {}
	for _, v in pairs(tab) do
		table.insert(newTab, fn(v))
	end
	return newTab
end

--- return table with only entries that return true on the function passed
function aa.Filter(tab, fn)
	local out = {}
	for k, v in pairs(tab) do
		if fn(v, k) then
			out[k] = v
		end
	end
	return out
end

--- return true if all entries return true on the function passed
function aa.Every(tab, fn)
	for k, v in pairs(tab) do
		if not fn(v, k) then
			return false
		end
	end
	return true
end

--- return true if at least one entry returns true on the function passed
function aa.Some(tab, fn)
	for k, v in pairs(tab) do
		if fn(v, k) then
			return true
		end
	end
	return false
end

--- return the first entry that returns true on the function passed
function aa.Find(tab, fn)
	for k, v in pairs(tab) do
		if fn(v, k) then
			return v
		end
	end
	return nil
end

--- return the index of the first entry that returns true on the function passed
function aa.FindIndex(tab, fn)
	for k, v in pairs(tab) do
		if fn(k, v) then
			return k
		end
	end
	return nil
end

-- -------------------------------------------------------------------------- --
-- ANCHOR                          Web Helpers                                --
-- -------------------------------------------------------------------------- --

-- string.match(str, '_VERSION = \'[%d]+[%.]-[%d]-[%.]-[%d]-\'')

--- Send an http request
function aa.SendHttpRequest(host, path, method, body, headers, parcel, successHandler, failureHandler)
	local success = nil
	local failure = nil
	if not successHandler then
		success = function (str, header, statuscode)
			d("HTTP Request: success.")
			d("HTTP Result Header: " .. header)
			d("HTTP Result StatusCode: " .. tostring(statuscode))

			local data = json.decode(str)
			if table.valid(data) then
				table.print(data)
			else
				d(str)
			end
			if parcel == "" then
				parcel = data
			end
		end
	end
	if not failureHandler then
		failure = function (error, header, statuscode)
			d("HTTP Failed Error: " .. error)
			d("HTTP Failed Header: " .. header)
			d("HTTP Failed StatusCode: " .. tostring(statuscode))
			if parcel == "" then
				parcel = {}
			end
		end
	end

	local params = {
		host = host,
		path = path,
		port = 443,
		method = method or "GET", -- "GET","POST","PUT","DELETE"
		https = true,
		onsuccess = successHandler or success,
		onfailure = failureHandler or failure,
		getheaders = true, -- true will return the headers, if you dont need it you can leave this at nil
		body = body or "", -- optional, if not required for your call can be nil or ""
		headers = headers or "", -- optional, if not required for your call can be nil or ""
	}

	HttpRequest(params)
end

--- Do a gitlab api info
function aa.GitlabRequest(method,endpoint,query,sHandler,fHandler)
	if not sHandler then d('[Anon Addons] Need success handler') return end
	if not fHandler then d('[Anon Addons] Need failure handler') return end
	aa.SendHttpRequest(
		'www.gitlab.com', '/api/v4/'..endpoint..query,
		method, "", "PRIVATE-TOKEN: "..aa._TOKEN, nil,
		sHandler, fHandler
	)
end

--- call the gitlab api and check the lastest tag/release of the given module
--@tparam string moduleTag Module short name
function aa.GetAnonModuleLatestVersion(moduleTag)
	local moduleInfo = assert(aa[moduleTag].GetInfo())
	aa.GitlabRequest('GET', 'projects/'..moduleInfo.Id..'/releases', '',
		function(str, header, statusCode)
			d('HTTP: '..tostring(statusCode))
			local data = json.decode(str)
			if table.valid(data) then
				aa[moduleTag]['latest'] = data[1].tag_name
				-- aa[moduleTag]['zipUrl'] = data[1].tag
			else
				d('Something went wrong with the request')
			end
		end, function(error, header, statusCode)
			d('TheFuck')
			d('HTTP: '..tostring(statusCode))
		end
	)
end

--- download latest zip from module
function aa.DownloadAnonModuleLatestZip(moduleTag)
	local moduleInfo = assert(aa[moduleTag].GetInfo())
	aa.GitlabRequest('GET','projects/'..moduleInfo.Id..'/repository/archive.zip',"?sha="..aa[moduleTag].latest,
		function(str, header, statusCode, something)
			d('HTTP: '..statusCode)
			d(header)
			d(something)
			d(str)
			local data = json.decode(str)
			if table.valid(data) then
				d(data)
			else
				d('Something went wrong with the request')
			end
		end, function(error, header, statusCode)
		end
	)
end

-- -------------------------------------------------------------------------- --
--                             External Libraries                             --
-- -------------------------------------------------------------------------- --

-- aa.fzy = aa.Require(GetLuaModsPath() .. [[AACore/fzy_lua.lua]])